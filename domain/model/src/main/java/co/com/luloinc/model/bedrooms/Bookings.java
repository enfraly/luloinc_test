package co.com.luloinc.model.bedrooms;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder(toBuilder = true)
public class Bookings {
    private long id;
    private Long room_id;
    private LocalDate start_date;
    private LocalDate end_date;
    private LocalDate created_at;
    private Number price;
    private String guest_name;
    private String guest_email;
    private String guest_phone;
    private String guest_document;
}
