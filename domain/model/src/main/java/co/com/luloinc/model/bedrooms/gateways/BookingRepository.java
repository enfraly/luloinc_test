package co.com.luloinc.model.bedrooms.gateways;

import co.com.luloinc.model.bedrooms.Bookings;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface BookingRepository {
    Mono<Bookings> newBooking(Bookings bookings);

    Flux<Bookings> getByBedroomId(Long bedroomId);

    Flux<Bookings> checkAvailabilityDate (Bookings bookings);
}
