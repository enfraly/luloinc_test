package co.com.luloinc.model.bedrooms;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Guest {
    private Long id;
    private String name;
    private String email;
    private String phone;
    private String address;
    private String last_name;
    private String document;
}
