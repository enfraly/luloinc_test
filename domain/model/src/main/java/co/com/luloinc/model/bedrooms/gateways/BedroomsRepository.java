package co.com.luloinc.model.bedrooms.gateways;

import co.com.luloinc.model.bedrooms.Bedroom;
import co.com.luloinc.model.bedrooms.Bookings;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface BedroomsRepository {

    Flux<Bedroom> getAllBedrooms();

    Mono<Bedroom> newBedroom(Bedroom bedroom);

    Mono<Bookings> newBooking(Bookings bookings);
}
