package co.com.luloinc.model.bedrooms;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@SuperBuilder(toBuilder = true)
public class Bedroom {
    private Long id;
    private String room_name;
    private String description;
    private String room_code;
    private String max_guests;
    private Number current_price;
    private List<Bookings> bookings;
}
