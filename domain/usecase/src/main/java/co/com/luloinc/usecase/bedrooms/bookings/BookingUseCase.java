package co.com.luloinc.usecase.bedrooms.bookings;

import co.com.luloinc.model.bedrooms.Bookings;
import co.com.luloinc.model.bedrooms.gateways.BookingRepository;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

@RequiredArgsConstructor
public class BookingUseCase {
    private final BookingRepository bookingRepository;


    public Mono<Bookings> createBooking(Bookings bookings) {

        return bookingRepository.checkAvailabilityDate(bookings).hasElements().flatMap(isAvailable -> {
            if (isAvailable) {
                return Mono.error(new RuntimeException("The room is not available"));
            } else {
                return bookingRepository.newBooking(bookings);
            }
        });
    }
}
