package co.com.luloinc.usecase.bedrooms;

import co.com.luloinc.model.bedrooms.Bedroom;
import co.com.luloinc.model.bedrooms.gateways.BedroomsRepository;
import co.com.luloinc.model.bedrooms.gateways.BookingRepository;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
public class BedroomsUseCase {

    private final BedroomsRepository bedroomsRepository;
    private final BookingRepository bookingRepository;
    public Mono<Bedroom> createBedRoom(Bedroom bedroom) {

        System.out.println("Bedroom: " + bedroom);
        return bedroomsRepository.newBedroom(bedroom);
    }
    //Function to get all bedrooms with bookings
    public Flux<Bedroom> getBedrooms() {
    return bedroomsRepository.getAllBedrooms().flatMap(bedroom -> bookingRepository.getByBedroomId(bedroom.getId()).collectList().map(bookings -> {
        bedroom.setBookings(bookings);
        return bedroom;
    }));
    }

}
