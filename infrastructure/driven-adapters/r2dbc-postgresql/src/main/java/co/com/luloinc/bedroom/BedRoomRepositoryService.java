package co.com.luloinc.bedroom;

import co.com.luloinc.model.bedrooms.Bedroom;
import co.com.luloinc.model.bedrooms.Bookings;
import co.com.luloinc.model.bedrooms.gateways.BedroomsRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@AllArgsConstructor
public class BedRoomRepositoryService implements BedroomsRepository {
    private final BedRoomMapper mapper;
    private final BedRoomDataRepository bedRoomDataRepository;
    @Override
    public Flux<Bedroom> getAllBedrooms(){
            return bedRoomDataRepository.findAll()
                    .map(mapper::toEntity);
    }

    @Override
    public Mono<Bedroom> newBedroom(Bedroom bedroom) {
        return bedRoomDataRepository.save(mapper.toData(bedroom))
                .map(mapper::toEntity);
    }


    @Override
    public Mono<Bookings> newBooking(Bookings bookings) {
        return null;
    }
}
