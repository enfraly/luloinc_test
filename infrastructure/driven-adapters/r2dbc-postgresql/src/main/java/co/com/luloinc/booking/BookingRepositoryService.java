package co.com.luloinc.booking;

import co.com.luloinc.bedroom.BedRoomDataRepository;
import co.com.luloinc.bedroom.BedRoomMapper;
import co.com.luloinc.model.bedrooms.Bedroom;
import co.com.luloinc.model.bedrooms.Bookings;
import co.com.luloinc.model.bedrooms.gateways.BedroomsRepository;
import co.com.luloinc.model.bedrooms.gateways.BookingRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@AllArgsConstructor
public class BookingRepositoryService implements BookingRepository {
    private final BookingMapper mapper;
    private final BookingDataRepository bookingDataRepository;

    @Override
    public Mono<Bookings> newBooking(Bookings bookings) {
        return bookingDataRepository.save(mapper.toData(bookings))
                .map(mapper::toEntity);
    }

    @Override
    public Flux<Bookings> getByBedroomId(Long bedroomId) {
        return bookingDataRepository.findAllByRoomId(bedroomId)
                .map(mapper::toEntity);
    }

    @Override
    public Flux<Bookings> checkAvailabilityDate(Bookings bookings) {
        return bookingDataRepository.checkAvailabilityDate(bookings.getRoom_id(), bookings.getStart_date(), bookings.getEnd_date())
                .map(mapper::toEntity);
    }


}
