package co.com.luloinc.booking;

import co.com.luloinc.bedroom.BedroomData;
import co.com.luloinc.model.bedrooms.Bedroom;
import co.com.luloinc.model.bedrooms.Bookings;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BookingMapper {
    Bookings toEntity(BookingData data);
    BookingData toData(Bookings model);
}
