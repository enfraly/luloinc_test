package co.com.luloinc.booking;

import co.com.luloinc.bedroom.BedroomData;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

@Repository
public interface BookingDataRepository extends ReactiveCrudRepository<BookingData, Long> {
    //Function sql to validate is dates are avaible
    @Query(value = "SELECT * FROM tbl_booking WHERE room_id = :roomId AND ((:startDate BETWEEN start_date AND end_date) OR (:endDate BETWEEN start_date AND end_date))")
    Flux<BookingData> checkAvailabilityDate(@Param("roomId") Long idBedroom, @Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);


    //Function sql to find all bookings by id bedroom
    @Query(value = "SELECT * FROM tbl_booking WHERE room_id = :roomId")
    Flux<BookingData> findAllByRoomId(@Param("roomId") Long idBedroom);
}
