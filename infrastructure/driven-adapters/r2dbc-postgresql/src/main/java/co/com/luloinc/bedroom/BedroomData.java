package co.com.luloinc.bedroom;

import co.com.luloinc.booking.BookingData;
import co.com.luloinc.model.bedrooms.Bookings;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;
import java.io.Serializable;
import java.util.List;

@Table("tbl_bedrooms")
@Data
@NoArgsConstructor
public class BedroomData  implements Serializable  {
    @Id
    @Column("id")
    private Long id;
    @Column("room_name")
    private String room_name;
    @Column("description")
    private String description;
    @Column("room_code")
    private String room_code;
    @Column("current_price")
    private Number current_price;



}
