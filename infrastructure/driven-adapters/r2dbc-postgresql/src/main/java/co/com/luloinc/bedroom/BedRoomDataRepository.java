package co.com.luloinc.bedroom;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface BedRoomDataRepository extends ReactiveCrudRepository<BedroomData, Long> {

  //  Mono<Bedroom> newBedroom(Bedroom bedroom);


}
