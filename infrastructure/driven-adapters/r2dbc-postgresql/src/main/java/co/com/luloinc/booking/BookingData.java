package co.com.luloinc.booking;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.io.Serializable;
import java.time.LocalDate;

@Table("tbl_booking")
@Data
@NoArgsConstructor
public class BookingData implements Serializable {
    @Id
    @Column("id")
    private long id;
    @Column("room_id")
    private Long room_id;
    @Column("start_date")
    private LocalDate start_date;
    @Column("end_date")
    private LocalDate end_date;
    @Column("created_at")
    private String created_at;
    @Column("price")
    private Number price;
    @Column("guest_name")
    private String guest_name;
    @Column("guest_email")
    private String guest_email;
    @Column("guest_phone")
    private String guest_phone;
    @Column("guest_document")
    private String guest_document;

}
