package co.com.luloinc.bedroom;

import co.com.luloinc.model.bedrooms.Bedroom;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BedRoomMapper {
    Bedroom toEntity(BedroomData data);
    BedroomData toData(Bedroom model);
}
