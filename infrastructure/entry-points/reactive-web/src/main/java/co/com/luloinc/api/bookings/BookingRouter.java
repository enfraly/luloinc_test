package co.com.luloinc.api.bookings;

import co.com.luloinc.api.Handler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
public class BookingRouter {

    @Bean
    public RouterFunction<ServerResponse> BookingRouter(BookingHandler handler) {
        return route(POST("/api/v1/booking"), handler::newBooking);
    }
}

