package co.com.luloinc.api.bookings;

import co.com.luloinc.api.common.GenericHandler;
import co.com.luloinc.api.common.Response;
import co.com.luloinc.api.request.BookingRequest;
import co.com.luloinc.usecase.bedrooms.bookings.BookingUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
public class BookingHandler extends GenericHandler {
    private final BookingUseCase bookingUseCase;

        public Mono<ServerResponse> newBooking(ServerRequest serverRequest) {
            return serverRequest.bodyToMono(BookingRequest.class).flatMap(bookingRequest -> bookingUseCase.createBooking(bookingRequest.toModel()))
                    .flatMap(data -> ServerResponse.ok().bodyValue(new Response<>(data, HttpStatus.OK)))
                    .onErrorResume(e->this.errorResponse(e,"reqUUID"));

        }


}
