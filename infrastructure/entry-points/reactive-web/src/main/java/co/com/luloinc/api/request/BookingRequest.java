package co.com.luloinc.api.request;

import co.com.luloinc.model.bedrooms.Bookings;
import co.com.luloinc.model.bedrooms.Guest;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.validation.constraints.FutureOrPresent;
import java.time.LocalDate;

@Data
@NoArgsConstructor
public class BookingRequest {


    @NonNull
    private Long room_id;

    private Guest guest;

    @NonNull
    @FutureOrPresent
    private LocalDate start_date;
    @NonNull
    @FutureOrPresent
    private LocalDate end_date;
    @NonNull
    private Number price;

    public Bookings toModel() {
        return Bookings.builder()
                .room_id(room_id)
                .start_date(start_date)
                .end_date(end_date)
                .price(price)
                .guest_name(guest.getName())
                .guest_email(guest.getEmail())
                .guest_phone(guest.getPhone())
                .guest_document(guest.getDocument())
                .build();
    }
}
