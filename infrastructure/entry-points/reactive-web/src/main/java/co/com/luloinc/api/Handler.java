package co.com.luloinc.api;

import co.com.luloinc.api.common.GenericHandler;
import co.com.luloinc.api.common.Response;
import co.com.luloinc.model.bedrooms.Bedroom;
import co.com.luloinc.usecase.bedrooms.BedroomsUseCase;
import co.com.luloinc.api.request.BedroomRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
public class Handler extends GenericHandler {
    private final BedroomsUseCase bedroomsUseCase;





    public Mono<ServerResponse> newBedroomUsecase(ServerRequest serverRequest) {
    System.out.println("newBedroomUsecase");
        return serverRequest.bodyToMono(BedroomRequest.class)
                .flatMap(bedroomRequest -> bedroomsUseCase.createBedRoom(bedroomRequest.toModel()))
                .flatMap(data -> bodyBuilder().bodyValue(new Response<>(data, HttpStatus.OK)))
                .onErrorResume(e->this.errorResponse(e,"reqUUID"));
    }

    public Mono<ServerResponse> getBedrooms(ServerRequest serverRequest) {
        return bedroomsUseCase.getBedrooms()
                .collectList()
                .flatMap(data -> bodyBuilder().bodyValue(new Response<>(data, HttpStatus.OK)))
                .onErrorResume(e->this.errorResponse(e,"reqUUID"));
    }
}
