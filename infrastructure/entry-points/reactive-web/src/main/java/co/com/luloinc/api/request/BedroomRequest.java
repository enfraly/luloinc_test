package co.com.luloinc.api.request;

import co.com.luloinc.model.bedrooms.Bedroom;
import co.com.luloinc.model.bedrooms.Guest;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class BedroomRequest {

    @NonNull
    private String room_name;
    @NonNull
    private String description;
    @NonNull
    private Number current_price;

    @NotNull
    private Guest host;


    public Bedroom toModel() {
        return Bedroom.builder()
                .room_name(room_name)
                .description(description)
                .current_price(current_price)
                .build();
    }
}

